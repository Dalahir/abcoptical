<?php
namespace ABC\AdminBundle\Repository;

use Doctrine\ORM\EntityRepository;

class CustomerRepository extends EntityRepository
{
	public function getNumberAppointments()
	{
		return 0;
	}
}
