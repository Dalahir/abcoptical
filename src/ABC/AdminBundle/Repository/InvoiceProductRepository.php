<?php
namespace ABC\AdminBundle\Repository;

use Doctrine\ORM\EntityRepository;

class InvoiceProductRepository extends EntityRepository
{
	public function numberInvoicesContainingProduct($product)
	{
		return count($this->getEntityManager()
            ->createQuery('SELECT p FROM ABCAdminBundle:InvoiceProduct p WHERE p.product = :product')
            ->setParameter(':product', $product)
            ->getResult());
	}
}
