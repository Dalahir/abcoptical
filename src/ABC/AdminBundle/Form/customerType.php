<?php
namespace ABC\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CustomerType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	$builder->add('firstName', 'text', array(
    		'label' => 'First Name: '
    	));

    	$builder->add('lastName', 'text', array(
    		'label' => 'Last Name: '
    	));

    	$builder->add('email', 'text', array(
    		'label' => 'Email: '
    	));

    	$builder->add('streetAddress', 'text', array(
    		'label' => 'Street: '
    	));

    	$builder->add('suburb', 'text', array(
    		'label' => 'Suburb: '
    	));

    	$builder->add('postCode', 'text', array(
    		'label' => 'Post Code: '
    	));

    	$builder->add('homePhone', 'text', array(
    		'label' => 'Contact Number'
    	));

    	$builder->add('mobilePhone', 'text', array(
    		'label' => 'Secondary Number: '
    	));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
    	$resolver->setDefaults(array(
            'data_class' => 'ABC\AdminBundle\Entity\Customer'
        ));
    }

    public function getName()
    {
    	return 'customerType';
    }
}