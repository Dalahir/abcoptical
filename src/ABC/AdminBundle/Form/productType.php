<?php
namespace ABC\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProductType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	$builder->add('name', 'text', array(
    		'label' => 'Product Name: '
    	));

    	$builder->add('price', 'text', array(
    		'label' => 'Price: '
    	));

    	$builder->add('stock', 'text', array(
    		'label' => 'Stock: '
    	));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
    	$resolver->setDefaults(array(
            'data_class' => 'ABC\AdminBundle\Entity\Product'
        ));
    }

    public function getName()
    {
    	return 'productForm';
    }
}