<?php
namespace ABC\AdminBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use ABC\AdminBundle\Entity\Customer;

class LoadCustomerData extends AbstractFixture implements OrderedFixtureInterface
{
	/**
	 * {@inheritDoc}
	 */
	public function load(ObjectManager $manager)
	{
		// create and persist fixtures
		$NUM_FIXTURES = 13;
		for($i = 0; $i < $NUM_FIXTURES; $i++)
		{
			$customer[$i] = new Customer();
			$manager->persist($customer[$i]);
		}

		// add data to instances
		$customer[0]->setFirstName('Natalie');
		$customer[0]->setLastName('Goddard');
		$customer[0]->setEmail('ngoddard@gmail.com');
		$customer[0]->setStreetAddress('18 Edward St');
		$customer[0]->setSuburb('Brisbane');
		$customer[0]->setPostCode('4001');
		$customer[0]->setHomePhone('0738109022');
		$customer[0]->setMobilePhone('0401209638');
		$this->addReference('Natalie', $customer[0]);

		$customer[1]->setFirstName('Yvette');
		$customer[1]->setLastName('Lyons');
		$customer[1]->setEmail('yvette_lyon@hotmail.com');
		$customer[1]->setStreetAddress('24 Avoca St');
		$customer[1]->setSuburb('Yeronga');
		$customer[1]->setPostCode('4104');
		$customer[1]->setHomePhone('0738485782');
		$customer[1]->setMobilePhone('0413652378');
		$this->addReference('Yvette', $customer[1]);

		$customer[2]->setFirstName('Kathryn');
		$customer[2]->setLastName('Jenkinns');
		$customer[2]->setEmail('katjenkinns@iinet.net');
		$customer[2]->setStreetAddress('1/18 Dexter St');
		$customer[2]->setSuburb('Tennyson');
		$customer[2]->setPostCode('4105');
		$customer[2]->setHomePhone('0431096952');
		$this->addReference('Kathryn', $customer[2]);

		$customer[3]->setFirstName('Jennifer');
		$customer[3]->setLastName('Louise');
		$customer[3]->setEmail('jen.L@talktalk.net');
		$customer[3]->setStreetAddress('103 Davis Lane');
		$customer[3]->setSuburb('Brendale');
		$customer[3]->setPostCode('4500');
		$customer[3]->setHomePhone('0753201738');
		$customer[3]->setMobilePhone('0489459921');
		$this->addReference('Jennifer', $customer[3]);

		$customer[4]->setFirstName('Michelle');
		$customer[4]->setLastName('Turner');
		$customer[4]->setEmail('MTurner@optusnet.com.au');
		$customer[4]->setStreetAddress('29 Cascade Drive');
		$customer[4]->setSuburb('Underwood');
		$customer[4]->setPostCode('1411');
		$customer[4]->setHomePhone('0770731334');
		$customer[4]->setMobilePhone('0447789653');
		$this->addReference('Michelle', $customer[4]);

		$customer[5]->setFirstName('Ben');
		$customer[5]->setLastName('Hogan');
		$customer[5]->setPostCode('5410');
		$customer[5]->setHomePhone('0778831634');
		$this->addReference('Ben', $customer[5]);

		$customer[6]->setFirstName('Natasha');
		$customer[6]->setLastName('Smith');
		$customer[6]->setEmail('NSmithy@tpg.com.au');
		$customer[6]->setStreetAddress('56 Ascot Court');
		$customer[6]->setSuburb('Bundall');
		$customer[6]->setPostCode('4217');
		$customer[6]->setHomePhone('0792811317');
		$customer[6]->setMobilePhone('0415475042');
		$this->addReference('Natasha', $customer[6]);

		$customer[7]->setFirstName('Courtney');
		$customer[7]->setLastName('Gonsalves');
		$customer[7]->setEmail('gonsalves@iinet.net');
		$customer[7]->setStreetAddress('24/145 Snipe St');
		$customer[7]->setSuburb('Miami');
		$customer[7]->setPostCode('4220');
		$customer[7]->setHomePhone('0755490087');
		$customer[7]->setMobilePhone('0454750423');
		$this->addReference('Courtney', $customer[7]);

		$customer[8]->setFirstName('Jason');
		$customer[8]->setLastName('House');
		$customer[8]->setEmail('man_in_the_house@talktalk.net');
		$customer[8]->setStreetAddress('2 Carberry St');
		$customer[8]->setSuburb('Grange');
		$customer[8]->setPostCode('4051');
		$customer[8]->setHomePhone('0443881263');
		$this->addReference('Jason', $customer[8]);

		$customer[9]->setFirstName('Tony');
		$customer[9]->setLastName('Mathouse');
		$customer[9]->setPostCode('4509');
		$customer[9]->setHomePhone('0417286753');
		$this->addReference('Tony', $customer[9]);

		$customer[10]->setFirstName('Christine');
		$customer[10]->setLastName('Howard');
		$customer[10]->setEmail('christy043@hotmail.com');
		$customer[10]->setStreetAddress('128 Grandview Rd');
		$customer[10]->setSuburb('Pullenvale');
		$customer[10]->setPostCode('4069');
		$customer[10]->setHomePhone('0732352904');
		$customer[10]->setMobilePhone('0412368799');
		$this->addReference('Christine', $customer[10]);

		$customer[11]->setFirstName('Julia');
		$customer[11]->setLastName('Hammar');
		$customer[11]->setEmail('julia.hammar@bigpond.com');
		$customer[11]->setStreetAddress('76 Ontario Crescent');
		$customer[11]->setSuburb('Parkinson');
		$customer[11]->setPostCode('4115');
		$customer[11]->setHomePhone('0739772748');
		$customer[11]->setMobilePhone('0402324857');
		$this->addReference('Julia', $customer[11]);

		$customer[12]->setFirstName('James');
		$customer[12]->setLastName('Menon');
		$customer[12]->setEmail('jamie.menon@gmail.com');
		$customer[12]->setStreetAddress('34 Taylor St');
		$customer[12]->setSuburb('Windsor');
		$customer[12]->setPostCode('4030');
		$customer[12]->setHomePhone('0739217545');
		$customer[12]->setMobilePhone('0402952335');
		$this->addReference('James', $customer[12]);
	
		$manager->flush();
	}

	/**
	 * {@inheritDoc}
	 */
	public function getOrder()
	{
		return 3;
	}
}