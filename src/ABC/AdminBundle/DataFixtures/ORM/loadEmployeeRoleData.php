<?php
namespace ABC\AdminBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use ABC\AdminBundle\Entity\EmployeeRole;

class LoadEmployeeRoleData extends AbstractFixture implements OrderedFixtureInterface
{
	/**
	 * {@inheritDoc}
	 */
	public function load(ObjectManager $manager)
	{
		$role[0] = new EmployeeRole();
		$role[0]->setTitle('Optometrist');
		$this->addReference('Optometrist', $role[0]);

		$role[1] = new EmployeeRole();
		$role[1]->setTitle('Salesperson');
		$this->addReference('Salesperson', $role[1]);

		$manager->persist($role[0]);
		$manager->persist($role[1]);

		$manager->flush();
	}

	/**
	 * {@inheritDoc}
	 */
	public function getOrder()
	{
		return 1;
	}
}