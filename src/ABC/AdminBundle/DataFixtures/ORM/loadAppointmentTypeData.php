<?php
namespace ABC\AdminBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use ABC\AdminBundle\Entity\AppointmentType;

class LoadAppointmentTypeData extends AbstractFixture implements OrderedFixtureInterface
{
	/**
	 * {@inheritDoc}
	 */
	public function load(ObjectManager $manager)
	{
		// create and persist fixtures
		$NUM_FIXTURES = 3;
		for($i = 0; $i < $NUM_FIXTURES; $i++)
		{
			$appointmentType[$i] = new AppointmentType();
			$manager->persist($appointmentType[$i]);
		}

		// add data to instances
		$appointmentType[0]->setDescription("Eye test");
		$this->addReference('Eye test', $appointmentType[0]);

		$appointmentType[1]->setDescription("Prescription check up");
		$this->addReference('Prescription check up', $appointmentType[1]);

		$appointmentType[2]->setDescription("Contact lens fitting");
		$this->addReference('Contact lens fitting', $appointmentType[2]);

		$manager->flush();
	}

	/**
	 * {@inheritDoc}
	 */
	public function getOrder()
	{
		return 4;
	}
}