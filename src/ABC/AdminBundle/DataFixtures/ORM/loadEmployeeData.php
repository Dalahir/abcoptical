<?php
namespace ABC\AdminBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use ABC\AdminBundle\Entity\Employee;

class LoadEmployeeData extends AbstractFixture implements OrderedFixtureInterface
{
	/**
	 * {@inheritDoc}
	 */
	public function load(ObjectManager $manager)
	{
		// create and persist fixtures
		$NUM_FIXTURES = 12;
		for($i = 0; $i < $NUM_FIXTURES; $i++)
		{
			$employee[$i] = new Employee();
			$manager->persist($employee[$i]);
		}

		// add data to instances
		$employee[0]->setFirstName('Amanda');
		$employee[0]->setLastName('Cormer');
		$employee[0]->setDateEmployed(new \DateTime('2009-05-26'));
		$employee[0]->setEmployeeRole($this->getReference('Optometrist'));
		$employee[0]->setContactPhone('0738494512');
		$employee[0]->setPassword('ee26b0dd4af7e749aa1a8ee3c10ae9923f618980772e473f88');
		$this->addReference('Amanda', $employee[0]);

		$employee[1]->setFirstName('Dorothy');
		$employee[1]->setLastName('Schaaf');
		$employee[1]->setDateEmployed(new \DateTime('2009-05-26'));
		$employee[1]->setEmployeeRole($this->getReference('Optometrist'));
		$employee[1]->setContactPhone('0732589269');
		$employee[1]->setPassword('');
		$this->addReference('Dorothy', $employee[1]);

		$employee[2]->setFirstName('Evelyn');
		$employee[2]->setLastName('Temple');
		$employee[2]->setDateEmployed(new \DateTime('2009-07-26'));
		$employee[2]->setEmployeeRole($this->getReference('Optometrist'));
		$employee[2]->setContactPhone('0736182642');
		$employee[2]->setPassword('');
		$this->addReference('Evelyn', $employee[2]);

		$employee[3]->setFirstName('Samuel');
		$employee[3]->setLastName('Covarrubias');
		$employee[3]->setDateEmployed(new \DateTime('2009-07-20'));
		$employee[3]->setEmployeeRole($this->getReference('Salesperson'));
		$employee[3]->setContactPhone('0764987846');
		$employee[3]->setPassword('');

		$employee[4]->setFirstName('Joseph');
		$employee[4]->setLastName('Aston');
		$employee[4]->setDateEmployed(new \DateTime('2009-09-23'));
		$employee[4]->setEmployeeRole($this->getReference('Salesperson'));
		$employee[4]->setContactPhone('0415943950');
		$employee[4]->setPassword('');
		$this->addReference('Joseph', $employee[4]);

		$employee[5]->setFirstName('Ana');
		$employee[5]->setLastName('Weston');
		$employee[5]->setDateEmployed(new \DateTime('2009-10-12'));
		$employee[5]->setEmployeeRole($this->getReference('Salesperson'));
		$employee[5]->setContactPhone('0731029229');
		$employee[5]->setPassword('');
		$this->addReference('Ana', $employee[5]);

		$employee[6]->setFirstName('Kristi');
		$employee[6]->setLastName('Turman');
		$employee[6]->setDateEmployed(new \DateTime('2010-05-10'));
		$employee[6]->setEmployeeRole($this->getReference('Salesperson'));
		$employee[6]->setContactPhone('0739602721');
		$employee[6]->setPassword('');
		$this->addReference('Kristi', $employee[6]);

		$employee[7]->setFirstName('Scott');
		$employee[7]->setLastName('Thurber');
		$employee[7]->setDateEmployed(new \DateTime('2010-08-23'));
		$employee[7]->setEmployeeRole($this->getReference('Salesperson'));
		$employee[7]->setContactPhone('0458926989');
		$employee[7]->setPassword('');

		$employee[8]->setFirstName('Richard');
		$employee[8]->setLastName('Weathers');
		$employee[8]->setDateEmployed(new \DateTime('2010-09-23'));
		$employee[8]->setEmployeeRole($this->getReference('Optometrist'));
		$employee[8]->setContactPhone('0241829918');
		$employee[8]->setPassword('');
		$this->addReference('Richard', $employee[8]);

		$employee[9]->setFirstName('Nicholas');
		$employee[9]->setLastName('Cutter');
		$employee[9]->setDateEmployed(new \DateTime('2010-02-07'));
		$employee[9]->setEmployeeRole($this->getReference('Optometrist'));
		$employee[9]->setContactPhone('0473603388');
		$employee[9]->setPassword('');
		$this->addReference('Nicholas', $employee[9]);

		$employee[10]->setFirstName('Susan');
		$employee[10]->setLastName('Tarrant');
		$employee[10]->setDateEmployed(new \DateTime('2011-06-27'));
		$employee[10]->setEmployeeRole($this->getReference('Salesperson'));
		$employee[10]->setContactPhone('0460876290');
		$employee[10]->setPassword('');

		$employee[11]->setFirstName('Jane');
		$employee[11]->setLastName('Cormer');
		$employee[11]->setDateEmployed(new \DateTime('2009-05-26'));
		$employee[11]->setEmployeeRole($this->getReference('Optometrist'));
		$employee[11]->setContactPhone('0738494512');
		$employee[11]->setPassword('');
		$this->addReference('Jane', $employee[11]);

		$manager->flush();
	}

	/**
	 * {@inheritDoc}
	 */
	public function getOrder()
	{
		return 2;
	}
}