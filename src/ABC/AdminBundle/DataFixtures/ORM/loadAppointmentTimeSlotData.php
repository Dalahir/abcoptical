<?php
namespace ABC\AdminBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use ABC\AdminBundle\Entity\AppointmentTimeSlot;

class LoadAppointmentTimeSlotData extends AbstractFixture implements OrderedFixtureInterface
{
	/**
	 * {@inheritDoc}
	 */
	public function load(ObjectManager $manager)
	{
		// create four time slots
		$NUM_TIME_SLOTS = 4;
		for($i = 0; $i < $NUM_TIME_SLOTS; $i++)
		{
			// make appointment slots
			$timeSlot[$i] = new AppointmentTimeSlot();
			
			// persist each slot
			$manager->persist($timeSlot[$i]);
		}

		// load a description into each time slot
		$timeSlot[0]->setDescription('Early morning');
		$this->addReference('Early morning', $timeSlot[0]);

		$timeSlot[1]->setDescription('Late morning');
		$this->addReference('Late morning', $timeSlot[1]);
		
		$timeSlot[2]->setDescription('Noon');
		$this->addReference('Noon', $timeSlot[2]);

		$timeSlot[3]->setDescription('Afternoon');
		$this->addReference('Afternoon', $timeSlot[3]);

		$manager->flush();
	}

	/**
	 * {@inheritDoc}
	 */
	public function getOrder()
	{
		return 5;
	}
}