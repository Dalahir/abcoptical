<?php
namespace ABC\AdminBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use ABC\AdminBundle\Entity\Invoice;

class LoadInvoiceData extends AbstractFixture implements OrderedFixtureInterface
{
	/**
	 * {@inheritDoc}
	 */
	public function load(ObjectManager $manager)
	{
		// create and persist fixtures
		$NUM_FIXTURES = 3;
		for($i = 0; $i < $NUM_FIXTURES; $i++)
		{
			$invoice[$i] = new Invoice();
			$manager->persist($invoice[$i]);
		}

		// add data to instances
		$invoice[0]->setTimeBilled(new \DateTime('2012-02-24 14:17:00'));
		$invoice[0]->setSeller($this->getReference('Joseph'));
		$invoice[0]->setCustomer($this->getReference('Ben'));
		$this->addReference('invoice1', $invoice[0]);

		$invoice[1]->setTimeBilled(new \DateTime('2011-07-20 16:32:00'));
		$invoice[1]->setSeller($this->getReference('Kristi'));
		$invoice[1]->setCustomer($this->getReference('Kathryn'));
		$this->addReference('invoice2', $invoice[1]);

		$invoice[2]->setTimeBilled(new \DateTime('2011-07-11 12:18:00'));
		$invoice[2]->setSeller($this->getReference('Ana'));
		$invoice[2]->setCustomer($this->getReference('Courtney'));
		$this->addReference('invoice3', $invoice[2]);;

		$manager->flush();
	}

	/**
	 * {@inheritDoc}
	 */
	public function getOrder()
	{
		return 8;
	}
}