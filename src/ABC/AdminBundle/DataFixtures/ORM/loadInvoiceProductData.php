<?php
namespace ABC\AdminBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use ABC\AdminBundle\Entity\InvoiceProduct;

class LoadInvoiceProductData extends AbstractFixture implements OrderedFixtureInterface
{
	/**
	 * {@inheritDoc}
	 */
	public function load(ObjectManager $manager)
	{
		// create fixtures, cannot persist until IDs are made
		$NUM_FIXTURES = 6;
		for($i = 0; $i < $NUM_FIXTURES; $i++)
		{
			$invoiceProduct[$i] = new InvoiceProduct();
		}

		// add data to instances
		$invoiceProduct[0]->setInvoice($this->getReference('invoice1'));
		$invoiceProduct[0]->setProduct($this->getReference('Air Optix Aqua Multifocal'));
		$invoiceProduct[0]->setQuantity(1);
		$invoiceProduct[0]->setSalePrice(110.00);

		$invoiceProduct[1]->setInvoice($this->getReference('invoice1'));
		$invoiceProduct[1]->setProduct($this->getReference('Disposable lens cleaning kit'));
		$invoiceProduct[1]->setQuantity(1);
		$invoiceProduct[1]->setSalePrice(4.00);

		$invoiceProduct[2]->setInvoice($this->getReference('invoice2'));
		$invoiceProduct[2]->setProduct($this->getReference('Air Optix Aqua Multifocal'));
		$invoiceProduct[2]->setQuantity(1);
		$invoiceProduct[2]->setSalePrice(110.00);

		$invoiceProduct[3]->setInvoice($this->getReference('invoice2'));
		$invoiceProduct[3]->setProduct($this->getReference('Disposable lens cleaning kit'));
		$invoiceProduct[3]->setQuantity(2);
		$invoiceProduct[3]->setSalePrice(8.00);

		$invoiceProduct[4]->setInvoice($this->getReference('invoice3'));
		$invoiceProduct[4]->setProduct($this->getReference('Bill Bass Opal'));
		$invoiceProduct[4]->setQuantity(1);
		$invoiceProduct[4]->setSalePrice(229.00);

		$invoiceProduct[5]->setInvoice($this->getReference('invoice3'));
		$invoiceProduct[5]->setProduct($this->getReference('Single vision lenses'));
		$invoiceProduct[5]->setQuantity(1);
		$invoiceProduct[5]->setSalePrice(75.00);

		// persist fixtures
		for($i = 0; $i < $NUM_FIXTURES; $i++)
		{
			$manager->persist($invoiceProduct[$i]);
		}

		$manager->flush();
	}

	/**
	 * {@inheritDoc}
	 */
	public function getOrder()
	{
		return 9;
	}
}