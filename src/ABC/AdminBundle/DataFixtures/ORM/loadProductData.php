<?php
namespace ABC\AdminBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use ABC\AdminBundle\Entity\Product;

class LoadProductData extends AbstractFixture implements OrderedFixtureInterface
{
	/**
	 * {@inheritDoc}
	 */
	public function load(ObjectManager $manager)
	{
		// create and persist fixtures
		$NUM_FIXTURES = 24;
		for($i = 0; $i < $NUM_FIXTURES; $i++)
		{
			$product[$i] = new Product();
			$manager->persist($product[$i]);
		}

		// add data to instances
		$product[0]->setName('Air Optix Aqua Multifocal');
		$product[0]->setPrice(115.00);
		$product[0]->setStock(8);
		$this->addReference('Air Optix Aqua Multifocal', $product[0]);

		$product[1]->setName('Proclear Multifocal N');
		$product[1]->setPrice(120.00);
		$product[1]->setStock(10);

		$product[2]->setName('Gelflex Ningaloo Colours');
		$product[2]->setPrice(79.00);
		$product[2]->setStock(4);

		$product[3]->setName('PureVision Sphere');
		$product[3]->setPrice(60.00);
		$product[3]->setStock(7);

		$product[4]->setName('Bill Bass Opal');
		$product[4]->setPrice(234.00);
		$product[4]->setStock(2);
		$this->addReference('Bill Bass Opal', $product[4]);

		$product[5]->setName('Timeless 8U440P');
		$product[5]->setPrice(179.00);
		$product[5]->setStock(2);

		$product[6]->setName('Bill Bass Polarised');
		$product[6]->setPrice(55.00);
		$product[6]->setStock(4);

		$product[7]->setName('Verge VE001, Bronze');
		$product[7]->setPrice(55.00);
		$product[7]->setStock(4);

		$product[8]->setName('Vitto Gotti VG801, Silver Purple');
		$product[8]->setPrice(129.00);
		$product[8]->setStock(3);

		$product[9]->setName('Brooks Brothers BB370T, 1224T Black');
		$product[9]->setPrice(139.00);
		$product[9]->setStock(2);

		$product[10]->setName('Single vision lenses');
		$product[10]->setPrice(80.00);
		$product[10]->setStock(20);
		$this->addReference('Single vision lenses', $product[10]);

		$product[11]->setName('Multifocal lenses');
		$product[11]->setPrice(156.00);
		$product[11]->setStock(15);


		$product[12]->setName('Prescription sunglass lenses');
		$product[12]->setPrice(88.00);
		$product[12]->setStock(10);

		$product[13]->setName('Glasses case Black');
		$product[13]->setPrice(5.00);
		$product[13]->setStock(10);

		$product[14]->setName('Glasses case Brown');
		$product[14]->setPrice(5.00);
		$product[14]->setStock(4);

		$product[15]->setName('Glasses case Blue');
		$product[15]->setPrice(5.00);
		$product[15]->setStock(4);

		$product[16]->setName('Glasses case Red');
		$product[16]->setPrice(5.00);
		$product[16]->setStock(8);

		$product[17]->setName('Glasses case Green');
		$product[17]->setPrice(5.00);
		$product[17]->setStock(11);

		$product[18]->setName('Purity Lens Cleaner Kit');
		$product[18]->setPrice(6.00);
		$product[18]->setStock(7);

		$product[19]->setName('Glasses chain Gold');
		$product[19]->setPrice(2.00);
		$product[19]->setStock(4);

		$product[20]->setName('Glasses chain Silver');
		$product[20]->setPrice(2.00);
		$product[20]->setStock(3);

		$product[21]->setName('Glasses chain Bronze');
		$product[21]->setPrice(2.00);
		$product[21]->setStock(4);

		$product[22]->setName('Disposable lens cleaning kit');
		$product[22]->setPrice(9.00);
		$product[22]->setStock(5);
		$this->addReference('Disposable lens cleaning kit', $product[22]);

		$product[23]->setName('Googly Glasses');
		$product[23]->setPrice(79.00);
		$product[23]->setStock(5);

		$manager->flush();
	}

	/**
	 * {@inheritDoc}
	 */
	public function getOrder()
	{
		return 7;
	}
}