<?php
namespace ABC\AdminBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use ABC\AdminBundle\Entity\Appointment;

class LoadAppointmentData extends AbstractFixture implements OrderedFixtureInterface
{
	/**
	 * {@inheritDoc}
	 */
	public function load(ObjectManager $manager)
	{
		// create and persist fixtures
		$NUM_FIXTURES = 10;
		for($i = 0; $i < $NUM_FIXTURES; $i++)
		{
			$appointment[$i] = new Appointment();
			$manager->persist($appointment[$i]);
		}

		// add data to instances
		$appointment[0]->setOptometrist($this->getReference('Amanda'));
		$appointment[0]->setDate(new \DateTime('2011-03-13'));
		$appointment[0]->setTimeSlot($this->getReference('Early morning'));
		$appointment[0]->setAppointmentType($this->getReference('Eye test'));
		$appointment[0]->setCustomer($this->getReference('Jennifer'));

		$appointment[1]->setOptometrist($this->getReference('Amanda'));
		$appointment[1]->setDate(new \DateTime('2012-03-22'));
		$appointment[1]->setTimeSlot($this->getReference('Early morning'));
		$appointment[1]->setAppointmentType($this->getReference('Eye test'));
		$appointment[1]->setCustomer($this->getReference('Jennifer'));

		$appointment[2]->setOptometrist($this->getReference('Dorothy'));
		$appointment[2]->setDate(new \DateTime('2011-07-11'));
		$appointment[2]->setTimeSlot($this->getReference('Late morning'));
		$appointment[2]->setAppointmentType($this->getReference('Prescription check up'));
		$appointment[2]->setCustomer($this->getReference('Courtney'));

		$appointment[3]->setOptometrist($this->getReference('Evelyn'));
		$appointment[3]->setDate(new \DateTime('2009-08-28'));
		$appointment[3]->setTimeSlot($this->getReference('Late morning'));
		$appointment[3]->setAppointmentType($this->getReference('Eye test'));
		$appointment[3]->setCustomer($this->getReference('Yvette'));

		$appointment[4]->setOptometrist($this->getReference('Evelyn'));
		$appointment[4]->setDate(new \DateTime('2010-03-13'));
		$appointment[4]->setTimeSlot($this->getReference('Noon'));
		$appointment[4]->setAppointmentType($this->getReference('Prescription check up'));
		$appointment[4]->setCustomer($this->getReference('Natalie'));

		$appointment[5]->setOptometrist($this->getReference('Evelyn'));
		$appointment[5]->setDate(new \DateTime('2010-08-28'));
		$appointment[5]->setTimeSlot($this->getReference('Noon'));
		$appointment[5]->setAppointmentType($this->getReference('Contact lens fitting'));
		$appointment[5]->setCustomer($this->getReference('Courtney'));

		$appointment[6]->setOptometrist($this->getReference('Richard'));
		$appointment[6]->setDate(new \DateTime('2009-08-28'));
		$appointment[6]->setTimeSlot($this->getReference('Early morning'));
		$appointment[6]->setAppointmentType($this->getReference('Eye test'));
		$appointment[6]->setCustomer($this->getReference('Natalie'));

		$appointment[7]->setOptometrist($this->getReference('Richard'));
		$appointment[7]->setDate(new \DateTime('2010-11-28'));
		$appointment[7]->setTimeSlot($this->getReference('Afternoon'));
		$appointment[7]->setAppointmentType($this->getReference('Eye test'));
		$appointment[7]->setCustomer($this->getReference('Julia'));

		$appointment[8]->setOptometrist($this->getReference('Nicholas'));
		$appointment[8]->setDate(new \DateTime('2011-07-20'));
		$appointment[8]->setTimeSlot($this->getReference('Afternoon'));
		$appointment[8]->setAppointmentType($this->getReference('Contact lens fitting'));
		$appointment[8]->setCustomer($this->getReference('Kathryn'));

		$appointment[9]->setOptometrist($this->getReference('Nicholas'));
		$appointment[9]->setDate(new \DateTime('2012-02-29'));
		$appointment[9]->setTimeSlot($this->getReference('Early morning'));
		$appointment[9]->setAppointmentType($this->getReference('Contact lens fitting'));
		$appointment[9]->setCustomer($this->getReference('Kathryn'));

		$manager->flush();
	}

	/**
	 * {@inheritDoc}
	 */
	public function getOrder()
	{
		return 6;
	}
}