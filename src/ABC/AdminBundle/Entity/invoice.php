<?php
namespace ABC\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
* @ORM\Entity(repositoryClass="ABC\AdminBundle\Repository\InvoiceRepository")
* @ORM\Table(name="invoices")
*/
class Invoice
{
	/**
	* @ORM\Id
	* @ORM\Column(type="integer")
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $id;

	/**
	 * @ORM\Column(type="time")
	 */
	protected $timeBilled;

	/**
	 * @ORM\ManyToOne(targetEntity="Employee")
	 */
	protected $seller;

	/**
     * @ORM\ManyToOne(targetEntity="Customer")
     */
	protected $customer;

	public function getTimeBilled()
	{
	    return $this->timeBilled;
	}
	
	public function setTimeBilled($timeBilled)
	{
	    $this->timeBilled = $timeBilled;
	    return $this;
	}

	public function getSeller()
	{
	    return $this->seller;
	}
	
	public function setSeller($seller)
	{
	    $this->seller = $seller;
	    return $this;
	}

	public function getCustomer()
	{
	    return $this->customer;
	}
	
	public function setCustomer($customer)
	{
	    $this->customer = $customer;
	    return $this;
	}
}