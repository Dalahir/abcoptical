<?php
namespace ABC\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
* @ORM\Entity(repositoryClass="ABC\AdminBundle\Repository\CustomerRepository")
* @ORM\Table(name="customers")
*/
class Customer
{
	/**
	* @ORM\Id
	* @ORM\Column(type="integer")
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	protected $firstName;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	protected $lastName;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	protected $email;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	protected $streetAddress;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	protected $suburb;

	/**
	 * @ORM\Column(type="string", length=10)
	 */
	protected $postCode;

	/**
	 * @ORM\Column(type="string", length=30)
	 */
	protected $homePhone;

	/**
	 * @ORM\Column(type="string", length=30, nullable=true)
	 */
	protected $mobilePhone;

	/**
	 * @ORM\OneToMany(targetEntity="Appointment", mappedBy="customer")
	 */
	protected $appointments;

	public function getId()
	{
		return $this->id;
	}

	public function getFirstName()
	{
	    return $this->firstName;
	}
	
	public function setFirstName($firstName)
	{
	    $this->firstName = $firstName;
	    return $this;
	}

	public function getLastName()
	{
	    return $this->lastName;
	}
	
	public function setLastName($lastName)
	{
	    $this->lastName = $lastName;
	    return $this;
	}

	public function getEmail()
	{
	    return $this->email;
	}
	
	public function setEmail($email)
	{
	    $this->email = $email;
	    return $this;
	}

	public function getStreetAddress()
	{
	    return $this->streetAddress;
	}
	
	public function setStreetAddress($streetAddress)
	{
	    $this->streetAddress = $streetAddress;
	    return $this;
	}

	public function getSuburb()
	{
	    return $this->suburb;
	}
	
	public function setSuburb($suburb)
	{
	    $this->suburb = $suburb;
	    return $this;
	}

	public function getPostCode()
	{
	    return $this->postCode;
	}
	
	public function setPostCode($postCode)
	{
	    $this->postCode = $postCode;
	    return $this;
	}

	public function getHomePhone()
	{
	    return $this->homePhone;
	}
	
	public function setHomePhone($homePhone)
	{
	    $this->homePhone = $homePhone;
	    return $this;
	}

	public function getMobilePhone()
	{
	    return $this->mobilePhone;
	}
	
	public function setMobilePhone($mobilePhone)
	{
	    $this->mobilePhone = $mobilePhone;
	    return $this;
	}

	public function getAppointments()
	{
		return $this->appointments;
	}

	public function getNumberAppointments()
	{
		return count($this->appointments);
	}
}