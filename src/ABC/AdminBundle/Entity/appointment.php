<?php
namespace ABC\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
* @ORM\Entity(repositoryClass="ABC\AdminBundle\Repository\AppointmentRepository")
* @ORM\Table(name="appointments")
*/
class Appointment
{
	/**
	* @ORM\Id
	* @ORM\Column(type="integer")
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $id;

	/**
     * @ORM\ManyToOne(targetEntity="Employee")
     * @ORM\JoinColumn(name="optometrist_id", referencedColumnName="id")
     */
	protected $optometrist;

	/**
	 * @ORM\Column(type="date")
	 */
	protected $date;

	/**
     * @ORM\ManyToOne(targetEntity="AppointmentTimeSlot")
     */
	protected $timeSlot;

	/**
     * @ORM\ManyToOne(targetEntity="AppointmentType")
     */
	protected $appointmentType;

	/**
     * @ORM\ManyToOne(targetEntity="Customer")
     */
	protected $customer;

	public function getOptometrist()
	{
	    return $this->optometrist;
	}
	
	public function setOptometrist($optometrist)
	{
	    $this->optometrist = $optometrist;
	    return $this;
	}

	public function getDate()
	{
	    return $this->date;
	}
	
	public function setDate($date)
	{
	    $this->date = $date;
	    return $this;
	}

	public function getTimeSlot()
	{
	    return $this->timeSlot;
	}
	
	public function setTimeSlot($timeSlot)
	{
	    $this->timeSlot = $timeSlot;
	    return $this;
	}

	public function getAppointmentType()
	{
	    return $this->appointmentType;
	}
	
	public function setAppointmentType($appointmentType)
	{
	    $this->appointmentType = $appointmentType;
	    return $this;
	}

	public function getCustomer()
	{
	    return $this->customer;
	}
	
	public function setCustomer($customer)
	{
	    $this->customer = $customer;
	    return $this;
	}
}