<?php
namespace ABC\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
* @ORM\Entity(repositoryClass="ABC\AdminBundle\Repository\EmployeeRoleRepository")
* @ORM\Table(name="employee_roles")
*/
class EmployeeRole
{
	/**
	* @ORM\Id
	* @ORM\Column(type="integer")
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	protected $title;

	/**
	 * @ORM\OneToMany(targetEntity="Employee", mappedBy="employeeRole")
	 */
	protected $employees;

	public function getTitle()
	{
	    return $this->title;
	}
	
	public function setTitle($title)
	{
	    $this->title = $title;
	    return $this;
	}
}