<?php
namespace ABC\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
* @ORM\Entity(repositoryClass="ABC\AdminBundle\Repository\EmployeeRepository")
* @ORM\Table(name="employees")
*/
class Employee
{
	/**
	* @ORM\Id
	* @ORM\Column(type="integer")
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	protected $firstName;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	protected $lastName;

	/**
	 * @ORM\Column(type="date")
	 */
	protected $dateEmployed;

	/**
     * @ORM\ManyToOne(targetEntity="EmployeeRole", inversedBy="employees")
     */
    protected $employeeRole;

	/**
	 * @ORM\Column(type="string", length=20, nullable=true)
	 */
	protected $contactPhone;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	protected $password;

	public function getFirstName()
	{
	    return $this->firstName;
	}
	
	public function setFirstName($firstName)
	{
	    $this->firstName = $firstName;
	    return $this;
	}

	public function getLastName()
	{
	    return $this->lastName;
	}
	
	public function setLastName($lastName)
	{
	    $this->lastName = $lastName;
	    return $this;
	}

	public function getDateEmployed()
	{
	    return $this->dateEmployed;
	}
	
	public function setDateEmployed($dateEmployed)
	{
	    $this->dateEmployed = $dateEmployed;
	    return $this;
	}

	public function getEmployeeRole()
	{
	    return $this->employeeRole;
	}
	
	public function setEmployeeRole($employeeRole)
	{
	    $this->employeeRole = $employeeRole;
	    return $this;
	}

	public function getContactPhone()
	{
	    return $this->contactPhone;
	}
	
	public function setContactPhone($contactPhone)
	{
	    $this->contactPhone = $contactPhone;
	    return $this;
	}

	public function getPassword()
	{
	    return $this->password;
	}
	
	public function setPassword($password)
	{
	    $this->password = $password;
	    return $this;
	}
}