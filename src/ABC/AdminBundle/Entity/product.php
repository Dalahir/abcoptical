<?php
namespace ABC\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
* @ORM\Entity()
* @ORM\Table(name="products")
*/
class Product
{
	/**
	* @ORM\Id
	* @ORM\Column(type="integer")
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $id;

	/**
	 * @ORM\Column(type="string", length=256)
	 */
	protected $name;

	/**
	 * @ORM\Column(type="decimal", scale=2, precision=7, nullable=true)
	 */
	protected $price;

	/**
	 * @ORM\Column(type="integer", nullable=true)
	 */
	protected $stock;

	public function getId()
	{
		return $this->id;
	}

	public function getName()
	{
	    return $this->name;
	}
	
	public function setName($name)
	{
	    $this->name = $name;
	    return $this;
	}

	public function getPrice()
	{
	    return $this->price;
	}
	
	public function setPrice($price)
	{
	    $this->price = $price;
	    return $this;
	}

	public function getStock()
	{
	    return $this->stock;
	}
	
	public function setStock($stock)
	{
	    $this->stock = $stock;
	    return $this;
	}
}