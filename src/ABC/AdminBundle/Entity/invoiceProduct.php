<?php
namespace ABC\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
* @ORM\Entity(repositoryClass="ABC\AdminBundle\Repository\InvoiceProductRepository")
* @ORM\Table(name="invoices_products")
*/
class InvoiceProduct
{
	/**
	* @ORM\Id
	* @ORM\ManyToOne(targetEntity="Invoice", inversedBy="id")
	*/
	protected $invoice;

	/**
	 * @ORM\Id
	 * @ORM\ManyToOne(targetEntity="Product", inversedBy="id")
	 */
	protected $product;

	/**
	 * @ORM\Column(type="integer")
	 */
	protected $quantity;

	/**
	 * @ORM\Column(type="decimal", scale=2, precision=7)
	 */
	protected $salePrice;

	public function getInvoice()
	{
	    return $this->invoice;
	}
	
	public function setInvoice($invoice)
	{
	    $this->invoice = $invoice;
	    return $this;
	}

	public function getProduct()
	{
	    return $this->product;
	}
	
	public function setProduct($product)
	{
	    $this->product = $product;
	    return $this;
	}

	public function getQuantity()
	{
	    return $this->quantity;
	}
	
	public function setQuantity($quantity)
	{
	    $this->quantity = $quantity;
	    return $this;
	}

	public function getSalePrice()
	{
	    return $this->salePrice;
	}
	
	public function setSalePrice($salePrice)
	{
	    $this->salePrice = $salePrice;
	    return $this;
	}
}