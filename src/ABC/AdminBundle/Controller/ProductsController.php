<?php
namespace ABC\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use ABC\AdminBundle\Entity\Product;
use ABC\AdminBundle\Form\ProductType;

class ProductsController extends Controller
{
	/**
     * @Route("/products/", name="ProductsList")
     * @Template()
     */
    public function indexAction()
    {
    	// find all products and hand them to view for rendering
    	$em = $this->getDoctrine()->getEntityManager();
    	$products = $em->getRepository('ABCAdminBundle:Product')->findAll();
        return array('products' => $products);
    }

    /**
     * @Route("/product/create/", name="ProductCreate")
     * @Template()
     */
    public function createAction()
    {
    	// new product container
    	$product = new Product();
    	
    	// create form and bind to product container
    	$form = $this->createForm(new ProductType(), $product);

    	// check if this is a postback
    	$request = $this->getRequest();
    	if ($request->getMethod() == 'POST')
    	{
    		// bind POSTed data to the form and check that it's valid
    		$form->bindRequest($request);
    		if ($form->isValid())
    		{
    			$em = $this->getDoctrine()->getEntityManager();

    			// save product to database
    			$em->persist($product);
    			$em->flush();

    			$this->get('session')->setFlash('good', 'Product added successfully!');
    			return $this->redirect($this->generateUrl('ProductsList'));
    		}
    	}

    	return array('form' => $form->createView());
    }

    /**
     * @Route("/product/delete/{id}", name="ProductDelete")
     */
    public function deleteAction($id)
    {
    	// find the product being delete
    	$em = $this->getDoctrine()->getEntityManager();
    	$product = $em->getRepository('ABCAdminBundle:Product')->find($id);

    	if (!$product)	// invalid ID
    	{
    		throw $this->createNotFoundException('That product could not be found.');		
    	}

    	// ensure that product is not being used in an invoice record
    	if ($em->getRepository('ABCAdminBundle:InvoiceProduct')
    			->numberInvoicesContainingProduct($product) == 0)
    	{
    		$em->remove($product);
    		$em->flush();

    		$this->get('session')->setFlash('good', "Product deleted successfully!");
    	}
    	else
    	{
    		// product in use
    		$this->get('session')->setFlash('bad', "That product is recorded in an invoice and cannot be deleted.");
    	}

    	return $this->redirect($this->generateUrl('ProductsList'));
    }
}