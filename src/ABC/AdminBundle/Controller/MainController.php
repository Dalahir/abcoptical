<?php
namespace ABC\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class MainController extends Controller
{
    /**
     * @Route("/", name="Home")
     * @Template()
     */
    public function indexAction()
    {
    	// no home page information yet, redirect to products listing as an entry
    	// point to the site.
    	return $this->redirect($this->generateUrl('ProductsList'));
        return array();
    }
}
