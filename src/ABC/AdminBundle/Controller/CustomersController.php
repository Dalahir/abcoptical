<?php
namespace ABC\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use ABC\AdminBundle\Entity\Customer;
use ABC\AdminBundle\Form\CustomerType;

class CustomersController extends Controller
{
	/**
     * @Route("/customers/", name="CustomersList")
     * @Template()
     */
    public function indexAction()
    {
    	$em = $this->getDoctrine()->getEntityManager();
    	$customers = $em->getRepository('ABCAdminBundle:Customer')->findAll();

        return array('customers' => $customers);
    }

    /**
     * @Route("/customer/edit/{id}", name="CustomerEdit")
     * @Template()
     */
    public function editAction($id)
    {
    	$em = $this->getDoctrine()->getEntityManager();

    	// new customer container
    	$customer = $em->getRepository('ABCAdminBundle:Customer')->find($id);
    	if (!$customer)
    	{
    		// invalid customer ID
    		throw $this->createNotFoundException('That customer could not be found.');
    	}
    	
    	// create form and bind to product container
    	$form = $this->createForm(new CustomerType(), $customer);

    	// check if this is a postback
    	$request = $this->getRequest();
    	if ($request->getMethod() == 'POST')
		{
			// bind POSTed data to the form and check that it's valid
			$form->bindRequest($request);
			if ($form->isValid())
			{
				// save product to database
				$em->persist($customer);
				$em->flush();

				$this->get('session')->setFlash('good', 'Customer edited successfully!');
				return $this->redirect($this->generateUrl('CustomersList'));
			}
		}

		return array('form' => $form->createView(), 'customer' => $customer);
    }
}