ABC Optical Admin
========================

This project shows an example solution to the Database Cluster's Assignment 2.
Students are free to create the design of the site however they like, so long
as certain functionality requirements are met. 

Overview
--------

You will create the web interface for your business’ administrative site. The site will be used by staff to manage their clients, sales and bookings. Your website must have the necessary functionality to allow staff to perform specific tasks.

### Requirements
You are required to:

1. Review all information provided about your business.
2. Rename the database you created in Assignment 1 to assign2_your_name.
3. Create a website that connects to the assign2 database. Your website must meet all requirements detailed below.
4. Conduct testing on your website.

#### Design
* The design of your website will not be assessed. 
* Your website must demonstrate sufficient usability and accessibility to be used by the intended audience – your business’ staff. 
* You will be required to make decisions relating to the presentation of data and form controls.

#### Functionality
Your website must allow the user to perform the following actions:

* Show a listing of all products, in a table format. For each product, display at least:
     * name
     * price
     * stock OR description (dependant on business)
* Add a new product (package for Flash Imagery).
* Delete a product (package for Flash Imagery).
* Show a listing of all recorded customers, not in a table format. For each customer, display at least:
     * full name
     * email
     * address
     * total number of bookings that customer has recorded (past, present and future)
* Edit individual customers.

No validation of user input is required at this stage of your website’s development.